import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default class TopMenu extends Component {
  render() {
    return (
      <View>
        <View style={Styles.Main}>

          <View style={Styles.Menu}>
            <Text>Menu 1</Text>
          </View>

          <View style={Styles.Menu}>
            <Text>Menu 2</Text>
          </View>

        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  Main: {
    backgroundColor: 'blue',
    margin: 12,
    flex: 1,
    flexDirection: 'row'
  },
  Menu:{
    backgroundColor: 'white',
    margin: 12,
    width: '40%'
  }
});
