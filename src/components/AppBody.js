import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import Card from './common/Card';
import TopMenu from './TopMenu'

export default class AppBody extends Component {

  state = {
    carddata: [
      {
        title: 'Data From state 1',
        subtitle: 'subtitle 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjwmlWrGbDdO-EALcrWjrsRCJkeIgiRUpaRLxB-9Yfyxe2h3iQfg',
      },
      {
        title: 'Data From state 2',
        subtitle: 'subtitle 2',
        image:
          'http://kissfmmedan.com/wp-content/uploads/2019/07/kocheng-oren.png',
      },
      {
        title: 'Data From state 3',
        subtitle: 'subtitle 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjwmlWrGbDdO-EALcrWjrsRCJkeIgiRUpaRLxB-9Yfyxe2h3iQfg',
      },
      {
        title: 'Data From state 1',
        subtitle: 'subtitle 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjwmlWrGbDdO-EALcrWjrsRCJkeIgiRUpaRLxB-9Yfyxe2h3iQfg',
      },
      {
        title: 'Data From state 2',
        subtitle: 'subtitle 2',
        image:
          'http://kissfmmedan.com/wp-content/uploads/2019/07/kocheng-oren.png',
      },
      {
        title: 'Data From state 3',
        subtitle: 'subtitle 2',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjwmlWrGbDdO-EALcrWjrsRCJkeIgiRUpaRLxB-9Yfyxe2h3iQfg',
      },
    ],
  };

  render() {
    return (
      <ScrollView>
        <TopMenu/>
        <View>
          {
            this.state.carddata.map(data =>
              <View>
                <Card
                  title={data.title}
                  image={data.image}
                  subtitle={data.subtitle}
                />
              </View>
            )
          }
        </View>
      </ScrollView>
    );
  }
}
