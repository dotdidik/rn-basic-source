import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class AppHeader extends Component {
    render() {
        return (
            <View style={Styles.Header}>
                <Text style={Styles.TextHeader}> MY APP </Text>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    Header: {
        height: 50,
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    TextHeader: {
       fontWeight: 'bold',
       fontSize: 22 
    }
})
